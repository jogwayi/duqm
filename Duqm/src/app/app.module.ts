import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp,SafePipe } from './app.component';
import { Home } from '../pages/home/home';
import { EmergencyContact } from '../pages/contact.emergency/contact.emergency';
import { Menu } from '../pages/menu/menu';
import { Business } from '../pages/business/business';
import { Attraction } from '../pages/attraction/attraction';
import { Hotel } from '../pages/hotel/hotel';
import { Restaurant } from '../pages/restaurant/restaurant';
import { Tour } from '../pages/interesting.places/interesting.places';
import { HotelView } from '../pages/view.hotel/view.hotel';
import { SingleMap } from '../pages/single.map/map';
import { Nearby } from '../pages/nearby/nearby';
import { ReportIncidence } from '../pages/report.incidence/report.incidence';
import {ImagesTaken } from '../pages/report.incidence/images.modal';
import { SezardForm } from '../pages/sezad.form/sezad.form';
import { ContactSezad } from '../pages/contact.sezad/contact.sezad';
import { PetrolStationPage } from '../pages/petrol-station/petrol-station';
import { AtmBankPage } from '../pages/atm-bank/atm-bank';
import { ShoppingPage } from '../pages/shopping/shopping';
import { VerifyModalComponent } from '../components/verify-modal/verify-modal';
import { FlyToDuqmPage } from '../pages/fly-to-duqm/fly-to-duqm';


@NgModule({
  declarations: [
    MyApp,SafePipe, 
    Home,Menu,Hotel,HotelView,EmergencyContact,ImagesTaken,PetrolStationPage,AtmBankPage,ShoppingPage,
    Attraction,Business,SingleMap,Nearby,Tour,Restaurant,ContactSezad,ReportIncidence,SezardForm,
    FlyToDuqmPage,VerifyModalComponent
  ],
  imports: [
    IonicModule.forRoot(MyApp,{
      backButtonIcon: 'ios-arrow-back',
      backButtonText: 'Back',
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,Home,Menu,Hotel,HotelView,EmergencyContact,ContactSezad,ReportIncidence,SezardForm,ShoppingPage,
    Attraction,Business,SingleMap,Nearby,Tour,Restaurant,ImagesTaken,PetrolStationPage,AtmBankPage,
    FlyToDuqmPage,VerifyModalComponent
  ],
  providers: [
  ]
})
export class AppModule {}
