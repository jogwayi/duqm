import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import {Pipe,PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';
import { Attraction } from '../pages/attraction/attraction';
import { Home } from '../pages/home/home';
import { Menu } from '../pages/menu/menu';
//import { Nearby } from '../pages/nearby/nearby';
import { EmergencyContact } from '../pages/contact.emergency/contact.emergency';
import { Business } from '../pages/business/business';
//import {JsonpModule} from '@angular/http';

export declare var cordova:any;

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
@Component({
  templateUrl: 'app.html',
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Home;

  pages: Array<{title: string, component: any,notSidemenu?:boolean}>;

  constructor(public platform: Platform,private sanitizer: DomSanitizer) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Main Menu', component: Menu },
      { title: 'Attraction', component: Attraction },
      { title: 'Business', component: Business },
      { title: 'Report Incidence', component: Menu },
      { title: 'Contact SEZAD', component: Menu },
      { title: 'Emergency Contacts', component: EmergencyContact },
    ];

  }
  trustSrcUrl = function(data){
    return this.sanitizer.bypassSecurityTrustResourceUrl(data);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      window.open = cordova.InAppBrowser.open;
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      //console.log("Comes here")
      StatusBar.backgroundColorByHexString('#003f5d');
      Splashscreen.hide();
      if (cordova && cordova.plugins && cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
