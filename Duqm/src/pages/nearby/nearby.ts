import { Component,NgZone} from '@angular/core';
import 'rxjs/add/operator/map';

import { NavController,Platform,ViewController } from 'ionic-angular';
//import {Geolocation,GoogleMap,GoogleMapsMarkerOptions,GoogleMapsMarker,CameraPosition,GoogleMapsLatLng,GoogleMapsEvent} from 'ionic-native';

export declare var plugin:any;
export declare var google:any;
@Component({
  selector: 'page-nearby',
  templateUrl: 'nearby.html'
})
export class Nearby{
  //@ViewChild('map') mapElement;
   nearbyMap:any;
   placeholderMap:any;
   searchItem:string;
   service:any;
   infowindow:any;
   location:any;
   submitted:boolean=false;
   apiKey:string = 'AIzaSyBoANGh2TRptg0rymz4xVB-DQ5dhrG8tWc';
   placesUrl:string= 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location';
   ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Attractions');
  }
  constructor(private zone: NgZone,public navCtrl: NavController,public platform: Platform, public viewCtrl: ViewController) {    
     this.platform.ready().then(() => this.onPlatformReady());
      
  }
    ionViewDidEnter() {
      //this.searchPlaces();
      this.loadMap();     
    }
   
    onPlatformReady(): void {

    }
    loadMap(){
        var div = document.getElementById("map_canvas");
        this.nearbyMap = plugin.google.maps.Map.getMap(div);
        this.nearbyMap.addEventListener(plugin.google.maps.event.MAP_READY, () =>{
             this.nearbyMap.getMyLocation((location)=>{
                const pyrmont = location.latLng; 
                this.location = location.latLng;
                this.nearbyMap.moveCamera({
                  'target': pyrmont,
                  'tilt': 60,
                  'zoom': 18,
                  'bearing': 140
                },()=> {
                  this.nearbyMap.addMarker({
                    'position': location.latLng,
                    'title': 'Your current location'
                  }, function(marker) {
                    marker.showInfoWindow();
                  });
                  //call api and add markers from here
                });      
                this.searchPlaces(location.latLng.lat,location.latLng.lng);      
              }, (message)=>{
                  //alert(message)
              });                                    
          });    
    }
  searchPlaces(lat,lng) {
      //use the pure js method to get places
      var pyrmont = {lat: lat, lng: lng};
      this.placeholderMap = new google.maps.Map(document.getElementById('gmp'), {
          center: pyrmont,
          zoom: 17
        });
        var service = new google.maps.places.PlacesService(this.placeholderMap);
        service.nearbySearch({
          location: pyrmont,
          radius: 500,
          type: ['point_of_interest']
        }, (results, status, pagination) =>{
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
              return;
            } else {
              console.log(results);
              this.addMarkers(results)
            }
          });      
      /*
      should handle cors
      let url = this.placesUrl+'=-33.8670522,151.1957362&radius=500&type=point_of_interest&key='+this.apiKey;
      this.http.get(url)
      .map(res=>res.json() || {})
      .subscribe(res=>{
          alert(res);
      });*/

  }
  
  addMarkers(places){
    for (var i = 0, place; place = places[i]; i++) {
          var types = place.types.join(', ');
          const pyrmont = new plugin.google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
          this.nearbyMap.addMarker({
            'position': pyrmont,
            'title': place.name,
            snippet:'Rating: '+ (place.rating || '')+'\n'+'Vicinity: '+ place.vicinity+'\n'+'Category: '+ types.replace(/_/g,' '),
          }, function(marker) {
            //marker.showInfoWindow();
          });

    }
  }

  ionViewDidLeave() {
    this.nearbyMap.remove();
  }
  
}
