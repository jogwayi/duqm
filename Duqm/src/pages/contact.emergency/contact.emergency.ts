import { Component} from '@angular/core';
import { NavController,ViewController } from 'ionic-angular';
@Component({
  selector: 'page-contact.emergency',
  templateUrl: 'contact.emergency.html'
})
export class EmergencyContact{
  menu:Array<Object> = [
    {
      title: "Wali Office",
      description: "",
      number:'25427110'
    },
    {
      title: "Public Prosecution",
      description: "",
      number:'25427200'
    },
    {
      title: "Duqm Hospital Incidence",
      description: "",
      number:'99203890'
    },
    {
      title: "Water",
      description: "",
      number:'153'
    },
    {
      title: "Electricity",
      description: "",
      number:'154'
    },
    {
      title: "Police",
      description: "",
      number:'999'
    },
    {
      title: "Airport Flight",
      description: "999",
      number:'0096824519173'
    },
  ];
  ionViewWillEnter() {
    //this.viewCtrl.setBackButtonText('Main Menu');
  }
  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {
    
  }
  call(number){
    window.open('tel:'+number,'_system');
  }
   
}
