import { Component,OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { NavController,ViewController } from 'ionic-angular';

/*
  Generated class for the FlyToDuqm page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-fly-to-duqm',
  templateUrl: 'fly-to-duqm.html'
})
export class FlyToDuqmPage {
  menu:Array<Object>=[];
  ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Attractions');
    this.http.get("assets/json/fly-to-duqm.json").subscribe(res=>{
      this.menu=res.json();
    })
  }
  constructor(public navCtrl: NavController, private http:Http, private viewCtrl: ViewController) {
    
  }
  ionViewDidLoad() {
    
  }

  call(){
    window.open('tel:+0096824760830','_system');
  }
  mail(){
    window.open('mailto:duqmair@travelcity-oman.com','_system');
  }

}
