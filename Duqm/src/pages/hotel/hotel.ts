import { Component} from '@angular/core';
import { NavController,ViewController } from 'ionic-angular';
import { HotelView } from '../view.hotel/view.hotel';
@Component({
  selector: 'page-hotel',
  templateUrl: 'hotel.html'
})
export class Hotel{
  hotels:Array<Object> = [
    {
      title: "Crown Plaza Duqm",
      description: "luxury",
      icon: "assets/img/crowne_plaza_duqm.jpg",
      page:HotelView,
      rating:this.favoriteArray(3),
      location:{lat:19.56946,lng:57.701575},
      phone: "25214444",
      email : "info.cpduqm@ihg.com",      
      gallery:[
        {
          title: "Crown Plaza Duqm",
          description: "Some description ",
          image: "assets/img/crowne_plaza_duqm.jpg",        
        },
        {
          title: "Crown Plaza Duqm",
          description: "Some description ",
          image: "assets/img/crowne_plaza_duqm_haima_13.jpg",
        },
        {
          title: "Crown Plaza Duqm",
          description: "Some description Mid",
          image: "assets/img/ttv3qYOzSuSLCOCjAi4b_17561411.jpg",
        },
        {
          title: "Crown Plaza Duqm",
          description: "Some description Mid",
          image: "assets/img/J_crowne_plaza_duqm_hotel.jpg",
        },
      ]
    },
    {
      title: "Park Inn Daqum",
      description: "luxury",
      icon: "assets/img/C7xqbfcRAyXbr1nzTmqN_JNJPD_MG_07.jpg",
      page:HotelView,
      location:{lat:19.56946,lng:57.701575},
      rating:this.favoriteArray(5),
      phone: "96822085700",
      email: "reservations.duqm@rezidorparkinn.com",
      gallery:[
        {
          title: "Park Inn Daqum",
          description: "Some description ",
          image: "assets/img/C7xqbfcRAyXbr1nzTmqN_JNJPD_MG_07.jpg",        
        },
        {
          title: "Park Inn Daqum",
          description: "Some description ",
          image: "assets/img/Frp3J0TjTgGzHdSon7so_JNJPD_MG_05.jpg",
        },
        {
          title: "Park Inn Daqum",
          description: "Some description Mid",
          image: "assets/img/GaGTwD3HTueKArOTLDZ4_JNJPD_MG_02.jpg",
        },
        {
          title: "Park Inn Daqum",
          description: "Some description Mid",
          image: "assets/img/3smQ1tVTSSyNzlzEOAsB_JNJPD_pool.jpg",
        }]
    },
    {
      title: "City Hotel Daqum",
      description: "Mid",
      icon: "assets/img/SmJVP7wKTyeeVjczhS1f_44061892.jpg",
      page:HotelView,
      location:{lat:19.5941531,lng:57.6354598},
      phone: 25214444,
      email : "info.cpduqm@ihg.com",
      rating:this.favoriteArray(2),
      gallery:[
        {
          title: "Crown Plaza Duqm",
          description: "Some description ",
          image: "assets/img/SmJVP7wKTyeeVjczhS1f_44061892.jpg",        
        },
        {
          title: "Park Inn Daqum",
          description: "Some description ",
          image: "assets/img/City_Hotel_Duqm_photos_Exterior.JPEG",
        },
        {
          title: "City Hotel Daqum",
          description: "Some description Mid",
          image: "assets/img/City_Hotel_Duqm_photos_Exterior.JPEG",
        }
      ]
    },
  ];
  ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Attractions');
  }
  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {
    
  }
  goTo(page,params){
    params.previous='Hotels';
    if(page !==null){
     this.navCtrl.push(page,params);
     //this.navCtrl.setRoot(page);
    }
  }
  favoriteArray(num){
    return new Array(num);
  }
   
}
