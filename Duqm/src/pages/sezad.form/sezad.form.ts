import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { Http,Headers} from '@angular/http';
import { NavController,NavParams, ViewController,ModalController,Loading,LoadingController,ToastController } from 'ionic-angular';
import { EmailComposer } from 'ionic-native';
import { VerifyModalComponent } from '../../components/verify-modal/verify-modal';
@Component({
  selector: 'page-sezad.form',
  templateUrl: 'sezad.form.html',
  host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'"
   },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateY(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate(400)
      ]),
      transition('* => void', animate(400, style({transform: 'translateY(-100%)', opacity: 0})))
    ])
  ]
})
export class SezardForm{
  name:string='';
  email:string='';
  message:string='';
  mobile:string='';
  department:string='';
  loader:Loading;
  accountSid = 'AC7b60531a6db486800a8380c20f5bea52'; // Your Account SID from www.twilio.com/console
  authToken = '9e263456981a5a0d28a04d8cada571a0';   // Your Auth Token from www.twilio.com/console
  mobileVerified=false;
  ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Back');
  }
  ionViewDidEnter() {

  }
  constructor(private toastCtrl: ToastController,private modal:ModalController,private loadingCtrl: LoadingController,private http:Http,public navCtrl: NavController,public params:NavParams, public viewCtrl: ViewController) {
    console.log("Here now...");
  }

  randomCode(){
    return Math.floor(10000 +Math.random()*90000);
  }
/*https://api.twilio.com/2010-04-01/Accounts/AC7b60531a6db486800a8380c20f5bea52/Messages
https://AC7b60531a6db486800a8380c20f5bea52:9e263456981a5a0d28a04d8cada571a0@api.twilio.com/2010-04-01/Accounts/AC7b60531a6db486800a8380c20f5bea52/Messages  
*/  showVerification(code:number){
    let alertModal = this.modal.create(VerifyModalComponent,{code:code});
      alertModal.present();
      alertModal.onDidDismiss((data)=>{
          this.sendMail();
      })
  }
  sendVerificationCode(){
    this.loader = this.loadingCtrl.create({
      content: "Sending verification code for your phone number...",
    });
    this.loader.present();
    let code = this.randomCode();
    let headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    var base64Creds = btoa(this.accountSid + ":" + this.authToken);
    var auth = 'Basic ' + base64Creds;
    headers.append("Authorization", auth);
    try{
      this.http.post(
        "https://api.twilio.com/2010-04-01/Accounts/"+this.accountSid+'/Messages'
        , "To="+encodeURIComponent(this.mobile)+'&From='+encodeURIComponent("+19252309402")+'&Body=Please enter '+code+' to verify your mobile number on MyCity App'
        ,{headers:headers}
      )
      .subscribe(res=>{
        this.loader.dismiss();
          this.showVerification(code);
      },err=>{
        let toast = this.toastCtrl.create({
            message: "Invalid phone number. Ensure your phone number includes country code eg +009 000000000",
            position: 'bottom',
            showCloseButton:true,
            duration:1000
          });
          toast.present();
          this.loader.dismiss();
      });
    }catch(e){
      let toast = this.toastCtrl.create({
        message: "Invalid phone number. Ensure your phone number includes country code eg +009 000000000",
        position: 'bottom',
        showCloseButton:true,
        duration:1000
      });
      toast.present();
      this.loader.dismiss();
    }
  }

  sendMail(){
     this.loader = this.loadingCtrl.create({
      content: "Initiating email ...",
    });
    this.loader.present();
    let email ={
        to:      ['msdsbadi@icloud.com'],
        cc:      'abdulrahman.albadi@outlook.com',
        subject: 'Contact form from '+this.name,
        body:    '<h3>Contact form from: <i>'+this.name+"</i></h3>\
                  <br/>Subject: "+this.mobile+"\
                  <br/>Phone number:"+this.mobile+"\
                  <br/>Email: "+this.email+"\
                  <br/>Department: "+this.department+"\
                  <br/>Message: "+this.message+"\
                  ",
        attachments:[],
        isHtml:  true
    };
    //check if Email composer is available
    EmailComposer.isAvailable().then((available: boolean) =>{
      if(available) {
       // alert("Email composer available");
        //Now we know we can send
      }else{
        //alert("Email composer unavailable");
      }
    });
    EmailComposer.open(email).then(()=>{
      this.loader.dismiss();
    });
  }
  
   
}
