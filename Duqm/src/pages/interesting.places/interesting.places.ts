import { Component} from '@angular/core';
import { NavController,ViewController } from 'ionic-angular';
import { HotelView } from '../view.hotel/view.hotel';
@Component({
  selector: 'page-interesting.places',
  templateUrl: 'interesting.places.html'
})
export class Tour{
  hotels:Array<Object> = [
    {
      title: "Rock Zoo",
      description: "luxury",
      icon: "assets/img/rock_garden/161ab42336dd1b8c1b57c6251496b15f.jpg",
      page:HotelView,
      rating:this.favoriteArray(3),
      location:{lat:19.56946,lng:57.701575},
      phone: "25214444",
      email : "info.cpduqm@ihg.com",      
      gallery:[
        {
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/161ab42336dd1b8c1b57c6251496b15f.jpg",        
        },
        {
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/2222.jpg",
        },
        {
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/4444.jpg",
        },
        {
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/6666.jpg",
        },
        {
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/4444444.jpg",
        },{
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/99973906.jpg",
        },{
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/533762982.jpg",
        },{
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/119156294.jpg",
        },{
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/Duqm17.jpg",
        },{
          title: "Rock Zoo",
          description: "",
          image: "assets/img/rock_garden/e6ca941edaac802d1030df01456f1ccd.jpg",
        },
      ]
    },
    {
      title: "Beach",
      description: "luxury",
      icon: "assets/img/beach/02_47.jpg",
      page:HotelView,
      location:{lat:19.56946,lng:57.701575},
      rating:this.favoriteArray(5),
      phone: "",
      email: "",
      gallery:[
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/02_47.jpg",        
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/Ras_AlHadd2.jpg",        
        },
        {
          title: "Beach",
          description: " ",
          image: "assets/img/beach/8066m.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/184404.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/5823449_76_b_w.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/125222146.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/DSC04174_1024x768.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/dsc6605.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/DSCF3355feet1.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/masirah_west_2.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/Oman_beach.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/Oman_kitesurfing_52f22811038a84fc6c4a.jpg",
        },
        {
          title: "Beach",
          description: "",
          image: "assets/img/beach/shutterstock_136390082.jpg",
        },
        ]
    }    
  ];
   ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Attractions');
  }
  constructor(public navCtrl: NavController,public viewCtrl: ViewController) {
    
  }
  goTo(page,params){
    //console.log(this.pages[page]);
    if(page !==null){
     this.navCtrl.push(page,params);
     //this.navCtrl.setRoot(page);
    }
  }
  favoriteArray(num){
    return new Array(num);
  }
   
}
