import { Component} from '@angular/core';
import { NavController,ViewController } from 'ionic-angular';
import { HotelView } from '../view.hotel/view.hotel';
@Component({
  selector: 'page-restaurant',
  templateUrl: 'restaurant.html'
})
export class Restaurant{
  restaurants:Array<Object> = [
    {
      title: "Ocean Restaurant",
      description: "This all-day dining restaurant serves delicious Arabic and international fare, as well as refreshing drinks. We recommend sampling a variety of dishes at the Arabic buffet. The restaurant faces the beach, so guests enjoy stunning views.\
                    <br/><b>Ocean Restaurant opening hours:</b>\
                    <br/><i>Breakfast:</i>\
                    <br/>06:30 - 10:00 (Sun - Thu)\
                    <br/>06:30 -11:00 (Fri - Sat)\
                    <br/><i>Lunch:</i>\
                    <br/>Noon - 15:00 (Sun - Thu)\
                    <br/><i>Dinner:</i>\
                    <br/>19:00 - 23:00 (Sun - Thu)",
      icon: "",
      page:HotelView,
      rating:this.favoriteArray(3),
      location:{lat:19.5652315,lng:57.6265153},
      phone: "25214444",
      email : "info.cpduqm@ihg.com",      
      gallery:[
        
      ]
    },
    {
      title: "Suqe",
      description: "Breakfast, Brunch, Lunch, Dinner, Dessert",
      icon: "assets/img/suqe/10014143_246458255543277_6925638746590675435_o.jpg",
      page:HotelView,
      location:{lat:19.56946,lng:57.701575},
      rating:this.favoriteArray(5),
      phone: "96822085700",
      email: "reservations.duqm@rezidorparkinn.com",
      gallery:[
        {
          title: "Suqe",
          description: "",
          image: "assets/img/suqe/10014143_246458255543277_6925638746590675435_o.jpg",        
        },
        {
          title: "Suqe",
          description: "",
          image: "assets/img/suqe/1415242_195093300679773_548246824_o.jpg",
        },
        {
          title: "Suqe",
          description: "",
          image: "assets/img/suqe/1979932_236726699849766_641918834280058979_o.jpg",
        },
        {
          title: "Suqe",
          description: "",
          image: "assets/img/suqe/14516462_575296152659484_4557613765326501366_n.jpg",
        },
        {
          title: "Suqe",
          description: "",
          image: "assets/img/suqe/14708257_578281249027641_3921664410732270739_n.jpg",
        },
        {
          title: "Suqe",
          description: "",
          image: "assets/img/suqe/14711226_581908708664895_2146202794914760494_o.jpg",
        }
        ]
    },
    {
      title: "RBG Bar & Grill",
      description: "Depending on your tastes and schedule, you'll be able to find the perfect dining choice at RBG Duqm. Opt for fresh salads and burgers at RBG Bar and Grill.\
        <br/><b>Restaurant opening hours:</b>\
        <br/><i> Lunch:</i> Noon - 15:00 (Daily)\
        <br/><i> Dinner:</i> 19:00 - 23:00 (Daily)\
        <br/><b>Bar opening hours:</b>\
        <br/> Noon - 15:00, 18:00 - 23 :00 (Sat - Thu)\
        <br/>14:00 - 23:00",
      icon: "assets/img/rbg/Satellite.jpg",
      page:HotelView,
      location:{lat:19.5941531,lng:57.6354598},
      phone: "96822085700",
      email : "info.cpduqm@ihg.com",
      rating:this.favoriteArray(2),
      gallery:[
        {
          title: "RBG Bar & Grill",
          description: "",
          image: "assets/img/rbg/Satellite.jpg",        
        },
        {
          title: "RBG Bar & Grill",
          description: " ",
          image: "assets/img/rbg/2016_1_rgb_base.jpg",
        },
        {
          title: "RBG Bar & Grill",
          description: " ",
          image: "assets/img/rbg/RBG_Grill.jpg",
        },
        {
          title: "RBG Bar & Grill",
          description: "",
          image: "assets/img/rbg/2016_1_rgb_innerbig.jpg",
        },
        {
          title: "RBG Bar & Grill",
          description: "Some description Mid",
          image: "assets/img/rbg/11377702_515800721916020_748355805_n.jpg",
        },
        {
          title: "RBG Bar & Grill",
          description: "Some description Mid",
          image: "assets/img/rbg/CRhQdrEWoAAfYVd.jpg",
        },
        {
          title: "RBG Bar & Grill",
          description: "Some description Mid",
          image: "assets/img/rbg/RBG-Grill-restaurant-opens-at-Park-Inn-Duqm_StoryPicture.jpg",
        }
      ]
    },
  ];
  ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Attractions');
  }
  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {
    
  }
  goTo(page,params){
    params.previous='Restaurants';
    //console.log(this.pages[page]);
    if(page !==null){
     this.navCtrl.push(page,params);
     //this.navCtrl.setRoot(page);
    }
  }
  favoriteArray(num){
    return new Array(num);
  }
   
}
