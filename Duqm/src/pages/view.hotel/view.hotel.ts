import { Component} from '@angular/core';
import { NavController,NavParams,ViewController } from 'ionic-angular';
import { SingleMap } from '../single.map/map';

@Component({
  selector: 'page-hotel-view',
  templateUrl: 'view.hotel.html'
})
export class HotelView{
  data:any;
  constructor(public navCtrl: NavController,public params:NavParams, public viewCtrl: ViewController) {
    console.log(params);
    this.data = params.data
  }  
  favoriteArray(num){
    return new Array(num);
  }

  loadMap(){
    console.log(this.data);
     this.navCtrl.push(SingleMap,this.data);
     //this.navCtrl.setRoot(page);
  }
  call(){
     if(this.data.phone !==undefined && this.data.phone !==''){
        window.open('tel:'+this.data.phone.replace(" ",""));
     }
  }
  email(){
    if(this.data.email !==undefined && this.data.email !==''){
      window.open('mailto:'+this.data.email);
    }
  }
  ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Back');
   }
    
   
}
