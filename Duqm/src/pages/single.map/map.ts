import { Component,NgZone } from '@angular/core';
import { NavController,Platform,NavParams ,ViewController} from 'ionic-angular';
export declare var plugin:any;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class SingleMap{
  //keys are set in platforms config xml files
    //android key  AIzaSyDk-CzJbBvKLHoR67RbRghGt2aB_5GEJ5g
    //ios key  AIzaSyDPoT-gE5Eu2Yc0YFJfc_sB8Y95PfNj7bo
    //js key AIzaSyB1Bfd6XPZX_HC7rj2m1ElEEoYNdlX4KQs
    map:any;
    data:any;
    initialized:boolean=false;  
    constructor(public navCtrl: NavController,public viewCtrl: ViewController,private zone: NgZone, public platform: Platform,public params:NavParams) {
      this.data = params.data
      this.platform.ready().then(() => this.onPlatformReady());
    }
  
    ionViewDidEnter() {
      this.loadMap();
    }
    
    onPlatformReady(): void {

    }
    loadMap(){
        var div = document.getElementById("map");
        this.map = plugin.google.maps.Map.getMap(div);
        this.map.addEventListener(plugin.google.maps.event.MAP_READY, () =>{
              const pyrmont = new plugin.google.maps.LatLng(this.data.location.lat, this.data.location.lng);
              this.map.moveCamera({
                'target': pyrmont,
                'tilt': 60,
                'zoom': 18,
                'bearing': 140
              },()=> {
                this.map.addMarker({
                  position: pyrmont,
                  title: this.data.title,
                  snippet:this.data.description,
                }, (marker)=> {
                  marker.showInfoWindow();
                });
              });            
            }, (message)=>{
                alert(message)
            });                                       
    }
    ionViewDidLeave() {
     this.map.remove();
    }   
}


