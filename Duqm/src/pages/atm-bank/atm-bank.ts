import { Component,OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { NavController,ViewController } from 'ionic-angular';

import { HotelView } from '../view.hotel/view.hotel';
/*
  Generated class for the PetrolStation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-atm-bank',
  templateUrl: 'atm-bank.html'
})
export class AtmBankPage implements OnInit{

  institutions:Array<Object> = [];
  ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Attractions');
    this.http.get("assets/json/atm-banks.json").subscribe(res=>{
      console.log(res.json());
      this.institutions=res.json();
    })
  }
  constructor(public navCtrl: NavController, private http:Http, private viewCtrl: ViewController) {
    
  }

  ngOnInit(){
      
  }
  goTo(page,params){
    params.previous='Petrol Station';
    //console.log(this.pages[page]);
    if(page !==null){
     this.navCtrl.push(HotelView,params);
     //this.navCtrl.setRoot(page);
    }
  }
  favoriteArray(num){
    return new Array(num);
  }
   
}
