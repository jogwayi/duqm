import { Component} from '@angular/core';
import {NavParams,ViewController } from 'ionic-angular';

@Component({
   selector: 'page-taken.pictures',
  template: '<ion-header>\
  <ion-toolbar color="primary">\
    <ion-title>\
      Images Captured\
    </ion-title>\
    <ion-buttons start>\
      <button ion-button (click)="dismiss()">\
        <ion-icon color="close" class="close"></ion-icon>Close\
      </button>\
    </ion-buttons>\
  </ion-toolbar>\
</ion-header>\
<ion-content>\
<ion-card *ngFor="let m of images;let i=index">\
  <img [src]="m">\
  <ion-row>\
    <ion-col></ion-col>\
    <ion-col></ion-col>\
    <ion-col text-right>\
    <button ion-button color="danger" (click)="deleteImage(i)" small>\
        <ion-icon name="trash"></ion-icon>Delete\
      </button>\
    </ion-col>\
    </ion-row>\
  </ion-card>\
</ion-content>\
'
})

export class ImagesTaken {
images:Array<string>;
 constructor(public viewCtrl: ViewController,public params:NavParams) {
    console.log(params.data);
    this.images = params.data.images;
 }
deleteImage(i){
    this.images.splice(i,1);
}
 dismiss() {
   this.viewCtrl.dismiss(this.images);
 }
 show(data){
   alert(data);
 }

}