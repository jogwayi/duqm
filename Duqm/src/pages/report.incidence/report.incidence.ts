import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { NavController, AlertController,ViewController,ModalController,Loading,LoadingController,ToastController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { Http,Headers} from '@angular/http';
import {ImagesTaken } from './images.modal';
import { VerifyModalComponent } from '../../components/verify-modal/verify-modal';

export declare var cordova:any;
export declare var plugin:any;

@Component({
  selector: 'page-report.incidence',
  templateUrl: 'report.incidence.html',
  host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'"
   },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateY(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate(400)
      ]),
      transition('* => void', animate(400, style({transform: 'translateY(-100%)', opacity: 0})))
    ])
  ]
})
export class ReportIncidence{
  subject:string='';
  description:string='';
  images:Array<string>;
  location:any;
  mobile:string='';
  category:string='';
  priority:string='';
  locatorMap:any;
  loader:Loading;
  accountSid = 'AC7b60531a6db486800a8380c20f5bea52'; // Your Account SID from www.twilio.com/console
  authToken = '9e263456981a5a0d28a04d8cada571a0';   // Your Auth Token from www.twilio.com/console  
 ionViewWillEnter() {
    //this.viewCtrl.setBackButtonText('Main Menu');
  }
  constructor(private toastCtrl: ToastController,private alertCtrl: AlertController,private modal:ModalController,private loadingCtrl: LoadingController,private http:Http,public modalCtrl: ModalController,private navCtrl: NavController, public viewCtrl: ViewController) {
   this.images=[];
    this.location={lat:'',lng:''};
  }
  viewImages(){
      let profileModal = this.modalCtrl.create(ImagesTaken, { images: this.images });
        profileModal.onDidDismiss(data => {
          this.images = data;
          console.log(this.images);
        });
        profileModal.present();
  }
  takePicture(){
    console.log(this.images.length)
    if(this.images.length>=3){
        let toast = this.alertCtrl.create({
          message: 'You can take up to three pictures. <br/>\
          You can still remove ones you want to get rid of and take new ones',
          buttons: [
            {
              text: 'Close',
              handler: data => {
               
              }
            },
            {
              text: 'View Images',
              handler: data => {
                this.viewImages();
              }
            },
          ]
        });
        toast.present();
      return;
    }

    Camera.getPicture({quality:80}).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = imageData; 
      //  use base64:icon.png// for sending to mail app, data:image/jpeg;base64,'for preview
      this.images.push(base64Image);
      }, (err) => {
        // Handle error
       // alert(err);
      });
  }
  ionViewDidEnter() {
      //this.searchPlaces();
      this.pickMyLocation();     
  }
   pickMyLocation(){
     var div = document.getElementById("map_canvas");
        this.locatorMap = plugin.google.maps.Map.getMap(div);
        this.locatorMap.addEventListener(plugin.google.maps.event.MAP_READY, () =>{
             this.locatorMap.getMyLocation((location)=>{
                   this.location.lat= location.latLng.lat;
                   this.location.lng= location.latLng.lng;
             });
        });
   }
  randomCode(){
    return Math.floor(10000 +Math.random()*90000);
  }
 showVerification(code:number){
    let alertModal = this.modal.create(VerifyModalComponent,{code:code});
      alertModal.present();
      alertModal.onDidDismiss((data)=>{
          this.sendMail();
      })
  }
  sendVerificationCode(){
    this.loader = this.loadingCtrl.create({
      content: "Sending verification code for your phone number...",
    });
    this.loader.present();
    let code = this.randomCode();
    let headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    var base64Creds = btoa(this.accountSid + ":" + this.authToken);
    var auth = 'Basic ' + base64Creds;
    console.log(auth);
    headers.append("Authorization", auth);
    try{
      this.http.post(
        "https://api.twilio.com/2010-04-01/Accounts/"+this.accountSid+'/Messages'
        , "To="+encodeURIComponent(this.mobile)+'&From='+encodeURIComponent("+19252309402")+'&Body=Please enter '+code+' to verify your mobile number on MyCity App'
        ,{headers:headers}
      )
      .subscribe(res=>{
        this.loader.dismiss();
          this.showVerification(code);
      },err=>{
        let toast = this.toastCtrl.create({
            message: "Invalid phone number. Ensure your phone number includes country code eg +009 000000000",
            position: 'bottom',
            showCloseButton:true,
            duration:1000
          });
          toast.present();
          this.loader.dismiss();
      });
    }catch(e){
      let toast = this.toastCtrl.create({
        message: "Invalid phone number. Ensure your phone number includes country code eg +009 000000000",
        position: 'bottom',
        showCloseButton:true,
        duration:1000
      });
      toast.present();
      this.loader.dismiss();
    }
  }

  sendMail(){
     this.loader = this.loadingCtrl.create({
      content: "Initiating email ...",
    });
    this.loader.present();
     var attachments:Array<string>=[];
     for(var a=0;a<this.images.length;a++){
        attachments.push('base64:icon.png//'+this.images[a]);
     }
    cordova.plugins.email.open({
        to:      ['msdsbadi@icloud.com'],
        cc:      'abdulrahman.albadi@outlook.com',
        subject: this.subject+' by '+this.mobile,
        body:    '<h4>Incidence Report by  <i>: '+this.mobile+"</i></h4>\
                  <br/>Subject: "+this.subject+"\
                  <br/>Category: "+this.category+"\
                  <br/>Priority: "+this.priority+"\
                  <br/>Location: "+this.location.lat+', '+this.location.lng+"\
                  <br/>Description: "+this.description+"\
                  ",
        attachments:this.images,
        isHtml:  true
    }).then(()=>{
      this.loader.dismiss();
    });
  }
}


