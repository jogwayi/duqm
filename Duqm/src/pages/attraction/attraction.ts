import { Component} from '@angular/core';
import { NavController,ViewController,NavParams } from 'ionic-angular';
import { Hotel } from '../hotel/hotel';
import { Restaurant } from '../restaurant/restaurant';
import { Tour } from '../interesting.places/interesting.places';
import { Nearby } from '../nearby/nearby';
import { FlyToDuqmPage } from '../fly-to-duqm/fly-to-duqm';
import { ShoppingPage } from '../shopping/shopping';
import { AtmBankPage } from '../atm-bank/atm-bank';
import { PetrolStationPage } from '../petrol-station/petrol-station';
@Component({
  selector: 'page-attraction',
  templateUrl: 'attraction.html'
})
export class Attraction{
  menu:Array<Object> = [
    {
      title: "Hotels",
      description: "",
      icon: "assets/img/people.png",
      page:Hotel,
    },
    {
      title: "Restaurants",
      description: "",
      icon: "assets/img/Icons_Land_Points_Of_Interest_Restaurant_Blue_2.ico",
      page:Restaurant,
    },
    {
      title: "Fly to Duqm",
      description: "",
      icon: "assets/img/flight_ico1.png",
      page:FlyToDuqmPage,
    },
    {
      title: "Public Agencies",
      description: "",
      icon: "assets/img/icon_government_agencies2x.png",
      page:null,
    },
    {
      title: "Interesting Places",
      description: "",
      icon: "assets/img/tour_btn_tourBlue.png",
      page:Tour,
    },
    {
      title: "Petrol Station",
      description: "",
      icon: "assets/img/PetrolStation.jpg",
      page:PetrolStationPage,
    },
    {
      title: "ATMs and Banks",
      description: "",
      icon: "assets/img/requestnew.png",
      page:AtmBankPage,
    },
    {
      title: "Near me",
      description: "",
      icon: "assets/img/nearby22.png",
      page:Nearby,
    },
    {
      title: "Shopping",
      description: "",
      icon: "assets/img/blue_shopping_cart.jpg",
      page:ShoppingPage,
    },
  ];
  ionViewWillEnter() {
    //this.viewCtrl.setBackButtonText('Main Menu');
  }
  constructor(public navCtrl: NavController,public params:NavParams, public viewCtrl: ViewController) {
    
  }
  goTo(page){
    //console.log(this.pages[page]);
    if(page !==null){
     this.navCtrl.push(page,{previous:'Attraction'});
     //this.navCtrl.setRoot(page);
    }
  }
   
}
