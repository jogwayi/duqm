import { Component,ViewChild} from '@angular/core';
import { NavController,Slides } from 'ionic-angular';
import {Menu} from '../menu/menu'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class Home{
  @ViewChild('homeSlider') slider: Slides;
  lastSliderPage:boolean=false;
   pages:Object;
  slides = [
    {
      title: "",
      description: "Thank you for using the Duqm App. Watch the short video to learn more about the Duqm Special Economic Zone.",
      videourl:"https://www.youtube.com/embed/0YndpzU9Do8?rel=0&showinfo=0"
    },
    {
      title: "Slide Two",
      description: "<b>Slide Two</b> Long description here....  Long description here....  Long description here....  Long description here....  ",
      image: "assets/img/royalty_free_vector.jpg",
    },
    {
      title: "Slide Three",
      description: "<b>Slide Three</b> Long description here....  Long description here....  Long description here....  Long description here....  ",
      image: "assets/img/royalty_free_vector.jpg",
    },
  ];
  constructor(public navCtrl: NavController) {
    this.pages={
     menu:Menu
    };
  }
  onSlideChanged() {
    //let currentIndex = this.slider.getActiveIndex();
    this.lastSliderPage= this.slider.isEnd();
    //console.log("Current index is", currentIndex);
  }
  goTo(page){
    //console.log(this.pages[page]);
    //this.navCtrl.push(this.pages[page]);
    this.navCtrl.setRoot(this.pages[page]);
  }
   
}
