import { Component} from '@angular/core';
import { NavController,ViewController } from 'ionic-angular';
//import { Camera } from 'ionic-native';
import { SezardForm } from '../sezad.form/sezad.form';

@Component({
  selector: 'page-contact.sezad',
  templateUrl: 'contact.sezad.html'
})
export class ContactSezad{
  form:SezardForm;
  branch:string;
  ionViewWillEnter() {
    //this.viewCtrl.setBackButtonText('Main Menu');
  }
  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {
    
  }
  goToForm(page){
    window.open(page);
  }
  contactForm(){
    this.navCtrl.push(SezardForm);
  }
   
}
