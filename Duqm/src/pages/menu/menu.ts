import { Component} from '@angular/core';
import { NavController,NavParams,ViewController } from 'ionic-angular';
import { Attraction } from '../attraction/attraction';
import { EmergencyContact } from '../contact.emergency/contact.emergency';
import { Business } from '../business/business';
//import { Nearby } from '../nearby/nearby';
import { ReportIncidence } from '../report.incidence/report.incidence';
import { ContactSezad } from '../contact.sezad/contact.sezad';


@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})
export class Menu{
  menu:Array<Object> = [
    {
      title: "Attraction",
      description: "",
      icon: "assets/img/people.png",
      page:Attraction,
    },
    {
      title: "Business",
      description: "",
      icon: "assets/img/ecommerce_handshake_icon.png",
      page:Business,
    },
    {
      title: "Report Incidence",
      description: "",
      icon: "assets/img/inspector_icon_sign_a_set_of_twelve_vintage.jpg",
      page:ReportIncidence,
    },
    {
      title: "Contact SEZAD",
      description: "",
      icon: "assets/img/help_desk_icon_from_business.jpg",
      page:ContactSezad,
    },
    {
      title: "Emergency Contacts",
      description: "",
      icon: "assets/img/wEUTPTOzRJm2FSg94Gpf_contactus.png",
      page:EmergencyContact,
    },
  ];
  constructor(public navCtrl: NavController,public params:NavParams, public viewCtrl: ViewController) {
    
  }
  goTo(page){
    //console.log(this.pages[page]);
    if(page !==null){
     this.navCtrl.push(page,{previous:'Menu'});
     //this.navCtrl.setRoot(page);
    }
  }
   ionViewWillEnter() {
    this.viewCtrl.setBackButtonText('Home');
   }
    
   
}
