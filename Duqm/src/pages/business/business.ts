import { Component } from '@angular/core';
import { NavController,ViewController } from 'ionic-angular';
@Component({
  selector: 'page-business',
  templateUrl: 'business.html'
})
export class Business {
  menu: Array<Object> = [
    {
      title: "Duqm From the sky",
      description: "",
      icon: "assets/img/flight_ico1.png",
      page: null,
    },
    {
      title: "Drive to My Land",
      description: "",
      icon: "assets/img/premier_insurance_icons4.png",
      page: null,
    },
    {
      title: "Service Guide",
      description: "",
      icon: "assets/img/icon_lg_guides_blue1.png",
      page: null,
    },
    {
      title: "Service Request",
      description: "",
      icon: "assets/img/requestnew.png",
      page: null,
    },
    {
      title: "Statistics",
      description: "",
      icon: "assets/img/RPmj4NPhSStA04Oe4bJQ_stsatsss.png",
      page: null,
    },
  ];
  ionViewWillEnter() {
    //this.viewCtrl.setBackButtonText('Main Menu');
  }
  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {
    
  }
  goTo(page) {
    //window.open('tel:' + number, '_system');
  }

}
