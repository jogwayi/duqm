import { Component } from '@angular/core';
import { ViewController,NavParams,ToastController } from 'ionic-angular';

/*
  Generated class for the VerifyModal component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'verify-modal',
  templateUrl: 'verify-modal.html'
})
export class VerifyModalComponent {
 wrongCode=false;
 title:string='Number Verification';
 message:string='Please enter the number you received on sms to verify your phone number';
  //$text-color: #000099
  verificationCode:string='';
  constructor(private viewCtrl: ViewController, private params:NavParams,private toastCtrl: ToastController) {
    
  }
  turnOnWifi(){

  }
  turnOnData(){

  }
  dismiss(){
    this.viewCtrl.dismiss();
  }
  verify(){
    if(this.params.data['code']==this.verificationCode){
       this.viewCtrl.dismiss();
    }else{
      this.wrongCode=true;
      let toast = this.toastCtrl.create({
        message: "Invalid verification code",
        position: 'bottom',
        showCloseButton:true,
        duration:1000
      });
      toast.present();
    }
  }

}